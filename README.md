# Version 0.4.0

## Сервер:
- http://35.161.183.239/API/{api_method}/

## Header
#### Передается по всех запросах, кроме регистрации/авторизации
#### Поля:
- id: int,
- token: string // токен, полученный от запроса регистрации/авторизации

#### Response в случае ошибки (нет доступа к API):
```
{
	"error": -1
	"msg": "unauthorized"
}
```
#### Response в случае успеха - соответствует респонсу запрашиваемого метода

## Ошибки
#### Во всех методах структура такая:
```
{
	"error": -1
	"msg": "unauthorized"
}
```

# User

## 1. Обычная регистрация пользователя
#### Метод: POST
#### url: /user/register/

#### Параметры:
- name: string,
- email: string,
- password: string,

#### Response:
```
{
	"response":
	{
		userId: int,
		token: string,
		name: string,
		email: string
	}
}
```

## 2. Авторизация пользователя (по email;password)
#### Метод: POST
#### url: /user/auth/

#### Параметры:
- email: string,
- password: string

#### Response:
```
{
	"response":
	{
		userId: int,
		token: string,
		name: string,
		description: string,
		avatar: string,
		email: string
	}
}
```

## 3. Авторизация/регистрация пользователя по youtube-токену
#### Метод: POST
#### url: /user/yauth/

#### Параметры:
- yToken: string

#### Response:
```
{
	"response":
	{
		userId: int,
		youtubeId: string,
		token: string,
		name: string,
		description: string
	}
}
```

## 4. Поиск youtube-пользователя по имени
#### Метод: POST
#### url: /user/ysearch/

#### Параметры:
- name: string

#### Response:
```
{
	"response":
	[
		{
			userId: int,
			youtubeId: string,
			name: string,
			description: string,
			isRealUser: bool
		}
	]
}
```

## 5. Обновление профиля
#### Метод: POST
#### url: /user/update/

#### Параметры:
- name: string,
- avatar: file,
- description: string

#### Response:
```
{
	"error": bool,
	"response":	
	{
		avatar: string
	}
}
```

## 6. Получение профиля
#### Метод: GET
#### url: /user/{userId}

#### Параметры:
- userId: int

#### Response:
```
{
	"response":
	{
		userId: int,
		name: string,
		youtubeId: string,
		avatar: string,
		description: string,
		isRealUser: bool
		tasks: 
		[
			{
				taskId: int,
				from: {
					id: int,
					isYoutubeUser: bool,
					isRealUser: bool,
					youtubeId: string,
					name: string,
					avatar: string,
				},
				toId: int,
				taskTitle: string,
				taskDescription: string,
				taskPrice: float,
				taskExpires: datetime,
				taskFile: string,
				taskPreview: string,
				taskStatus: int,
				taskIsActive: bool,
				taskCreationDate: datetime,
				taskDoneAt: datetime
			}
		]
	}
}
```

# Task

## 1. Создание задачи
#### Метод: POST
#### url: /task/create

#### Параметры:
- youtubeId: string,
- taskTitle: string,
- taskDescription: string,
- taskPrice: float,
- taskExpires: datetime

#### Response:
```
{
	"response":
	{
		taskId: int,
		toId: int
	}
	// в случае ошибки
	error: int // 1 - попытка поставить задачу не youtube-юзеру, 2 - задача с таким title еще активна 3 - иная ошибка
	msg: string	
}
```

## 2. Подтверждение/отказ от задачи
#### Метод: POST
#### url: /task/confirm

#### Параметры:
- taskId: int,
- confirm: bool

#### Response:
```
{
	"response":
	{
		status: bool
	}
}
```

## 3. Прикрепить файл к задаче
#### Метод: POST
#### url: /task/addfile

#### Параметры (все - обязательные):
- taskId: int,
- taskFile: file,
- taskPreview: file

#### Response:
```
{
	"response":
	{
		taskFile: string,
		taskPreview: string
	}
}
```

## 4. Получение параметров задачи
#### Метод: GET
#### url: /task/{taskId}/

#### Параметры:
- taskId: int

#### Response
```
{
	"response":
	{
		taskId: int,
		from: {
			id: int,
			isYoutubeUser: bool
			youtubeId: string,
			name: string,
			avatar: string,
		}
		to: {
			id: int,
			youtubeId: string,
			isRealUser: bool,
			name: string
		},
		taskTitle: string,
		taskDescription: string,
		taskPrice: float,
		taskExpires: datetime,
		taskFile: string,
		taskStatus: int // 0 - новая задача, 1 - отказанная задача, 2 - принятая задача, 3 - выполненная задача
		taskCreationDate: datetime,
		taskDoneAt: datetime,
		taskPreview: string,
		lastComments:
		[
			from: {
				id: int,
				isYoutubeUser: bool
				youtubeId: string,
				name: string,
				avatar: string,
			},
			creationDate: datetime,
			comment: string
		]
	}
}
```

## 5. Список задач "мне и от меня"
#### Метод: GET
#### url: /task/

#### Response
```
{
	"response":
	{	
		"tasksFromMe":
		[
			{
				taskId: int,
				from: {
					id: int,
					isYoutubeUser: bool
					youtubeId: string,
					name: string,
					avatar: string,
				}
				to: {
					id: int,
					youtubeId: string,
					name: string
				},
				taskTitle: string,
				taskDescription: string,
				taskPrice: float,
				taskExpires: datetime,
				taskFile: string,
				taskStatus: int // 0 - новая задача, 1 - отказанная задача, 2 - принятая задача, 3 - выполненная задача
				taskCreationDate: datetime,
				taskDoneAt: datetime,
				taskPreview: string,
				lastComments:
				[
					from: {
						id: int,
						isYoutubeUser: bool
						youtubeId: string,
						name: string,
						avatar: string,
					},
					creationDate: datetime,
					comment: string,
					id: int
				]				
			}
		],
		"tasksToMe":
		[
			{
				taskId: int,
				from: {
					id: int,
					isYoutubeUser: bool
					youtubeId: string,
					name: string,
					avatar: string,
				}
				to: {
					id: int,
					youtubeId: string,
					name: string
				},
				taskTitle: string,
				taskDescription: string,
				taskPrice: float,
				taskExpires: datetime,
				taskFile: string,
				taskStatus: int // 0 - новая задача, 1 - отказанная задача, 2 - принятая задача, 3 - выполненная задача
				taskCreationDate: datetime,
				taskDoneAt: datetime,
				taskPreview: string,
				lastComments:
				[
					from: {
						id: int,
						isYoutubeUser: bool
						youtubeId: string,
						name: string,
						avatar: string,
					},
					creationDate: datetime,
					comment: string,
					id: int
				]				
			}
		]	
	}
}
```

# Comments

## 1. Добавить комментарий к задаче
#### Метод: POST
#### url: /comment/add/

#### Параметры:
- taskId: int,
- comment: string

#### Response:
```
{
	"response":
	{
		"id": int
		"comment": string
	}
}
```

## 2. Получить комментарии к задаче
#### Метод: GET
#### url: /comment/{taskId}/{limit}/{fromTime}/

#### Параметры:
- taskId: int
- limit: int
- fromTime: datetime

#### Response:
```
{
	"response":
	taskId: int,
	items:
	[
		{
			id: int,	
			comment: string,
			creationDate: datetime,
			from: {
				id: int,
				isYoutubeUser: bool
				youtubeId: string,
				name: string,
				avatar: string,
			}
		}
	]
}
```

# Feeds

## 1. Фид выполненных задач
#### Метод: GET
#### url: /feed/{limit}/{fromTime}/

#### Параметры:
- limit: int,
- fromTime: datetime

#### Response:
```
{
	"response":
	[
		{
			taskId: int,
			from: {
				id: int,
				isYoutubeUser: bool
				youtubeId: string,
				name: string,
				avatar: string,
			}
			to: {
				id: int,
				youtubeId: string,
				name: string
			},
			taskTitle: string,
			taskDescription: string,
			taskPrice: float,
			taskExpires: datetime,
			taskFile: string,
			taskStatus: int // 0 - новая задача, 1 - отказанная задача, 2 - принятая задача, 3 - выполненная задача
			taskCreationDate: datetime,
			taskDoneAt: datetime,
			taskPreview: string				
		}
	]
}
```